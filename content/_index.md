---
title: Home
description: >-
  software engineering for web apps, embedded systems, and mobile
hero: take a byte
subhero: seriously
menu:
  main:
    identifier: home
    title: home
    weight: 1
---