module.exports = {
  plugins: [
    require('postcss-import')({
      path: ["assets/css"],
    }),
    require('tailwindcss')('./assets/css/tailwind.js'),
    require('autoprefixer'),
  ]
}